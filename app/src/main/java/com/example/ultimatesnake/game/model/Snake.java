package com.example.ultimatesnake.game.model;

import android.graphics.Point;

import com.example.ultimatesnake.game.Direction;
import com.example.ultimatesnake.game.Rectangle;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A class which handles multiple SnakeSegments and allows for handling them like one single unit
 */
public class Snake {

    protected LinkedList<SnakeSegment> segments;
    int snakeWidth;
    Rectangle bounds;
    private int transitionCount = 0;
    private int startLength;

    public Snake(int startLength, int startWidth, Point startPosition, Direction startDirection) {
        segments = new LinkedList<>();
        this.startLength = startLength;
        segments.addFirst(new SnakeSegment(startLength, startWidth, startPosition, startDirection));
        this.snakeWidth = startWidth;
    }
    public Snake(int startLength, int startWidth, Point startPosition, Direction startDirection, Rectangle bounds) {
        this(startLength, startWidth, startPosition, startDirection);
        this.bounds = bounds;
    }

    /**
     * Moves the snake 1 in-game unit by extending its head and decreasing the length of its tail by that much.
     * If the snake has bounds and it touches one, it will loop back to the opposite bound.
     */
    public void move() {
        int moveAmount = 1;
        SnakeSegment head = segments.getFirst();
        head.moveHead(moveAmount);
        head.increaseLength(moveAmount);
        shrink(moveAmount);
        if (transitionCount > 0) transitionCount--;
        if (bounds != null) checkBounds();
    }

    /**
     * Checks if the snake's head is touching any bounds,
     * creates a new segment at the opposite bound if so and starts a transition phase while the head is looping through the bound. During this phase the snake's direction cannot be changed.
     */
    private void checkBounds() {
        SnakeSegment headSegment = segments.getFirst();
        Point head = headSegment.getHead();
        if (head.x < bounds.topLeft.x) {
            segments.addFirst(new SnakeSegment(0, headSegment.getWidth(), new Point(bounds.bottomRight.x, head.y), headSegment.getDirection()));
            transitionCount = snakeWidth;
        }
        else if (head.x > bounds.bottomRight.x) {
            segments.addFirst(new SnakeSegment(0, headSegment.getWidth(), new Point(bounds.topLeft.x - 1, head.y), headSegment.getDirection()));
            transitionCount = snakeWidth;
        }
        else if (head.y < bounds.topLeft.y) {
            segments.addFirst(new SnakeSegment(0, headSegment.getWidth(), new Point(head.x, bounds.bottomRight.y), headSegment.getDirection()));
            transitionCount = snakeWidth;
        }
        else if (head.y > bounds.bottomRight.y) {
            segments.addFirst(new SnakeSegment(0, headSegment.getWidth(), new Point(head.x, bounds.topLeft.y - 1), headSegment.getDirection()));
            transitionCount = snakeWidth;
        }
    }
    public Point getHead() {
        return segments.getFirst().getHead();
    }

    /**
     * @return The direction of the Snake's head segment
     */
    public Direction getDirection () {
        return segments.getFirst().getDirection();
    }

    /**
     * Grows the snake by extending its tail by the given amount
     * @param amount
     */
    public void grow(int amount) {
        segments.getLast().increaseLength(amount);
    }

    /**
     * Shrinks the snake's tail by the given amount. The snake cannot become shorter than its initial length.
     * @param amount
     * @throws IllegalStateException if the snake has reached its minimum length
     */
    public void shrink(int amount) {
        if (snakeLength() <= startLength) {
            throw new IllegalStateException("The snake is too small to continue shrinking");
        }

        segments.getLast().decreaseLength(amount);
        if (segments.getLast().getLength() <= 0)
            segments.removeLast();
    }

    /**
     * @return The length of the whole snake; every segment's length added together
     */
    public int snakeLength() {
        int length = 0;
        for (SnakeSegment segment : segments) {
            length += segment.getLength();
        }
        return length;
    }
    public List<Rectangle> getSegmentsOutlines() {
        List<Rectangle> outlines = new ArrayList<>();

        for (SnakeSegment segment : segments) {
            outlines.add(segment.getOutline());
        }
        return outlines;
    }

    /**
     * Checks if the snake's head segment is overlapping with the specified Rectangle
     * @param collisionObject
     * @return true if an overlap is found
     */
    public boolean checkCollision(Rectangle collisionObject) {
        Rectangle headSegment = segments.getFirst().getOutline();
        return headSegment.overlapsWith(collisionObject);
    }
    public int segmentCount() {
        return this.segments.size();
    }

    /**
     * Changes the head's direction by creating a new segment in place of the old head. Will not do anything if the snake is currently going through a transition phase, such as its head being in the process of looping through given bounds.
     * @param newDirection
     */
    public void setDirection(Direction newDirection) {
        if (transitionCount == 0) {
            SnakeSegment headSegment = segments.getFirst();
            Point newSegmentHead = headSegment.headWithChangedDirection(newDirection);
            if (newSegmentHead != null) {
                headSegment.shrinkHead(snakeWidth);
                segments.addFirst(new SnakeSegment(snakeWidth, snakeWidth, newSegmentHead, newDirection));
                transitionCount = snakeWidth;
            }
        }
    }

}

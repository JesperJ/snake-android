package com.example.ultimatesnake.game;

import android.graphics.Point;

/**
 * A class to represent a rectangle with different operations
 */
public class Rectangle extends AnimatedGraphic {
    public Point topLeft;
    public Point bottomRight;
    public Rectangle() {
        this.topLeft = new Point(0,0);
        this.bottomRight = new Point(0,0);
    }
    public Rectangle(Point top, Point bottom) {
        this.topLeft = top;
        this.bottomRight = bottom;
    }
    public Rectangle(Point top, Point bottom, int color) {
        this(top, bottom);
        setColor(color);
    }

    /**
     * Tar emot en rektangel för att göra detta till en kopia av den
     * @param newBounds
     */
    public void resizeTo(Rectangle newBounds) {
        this.topLeft = newBounds.topLeft;
        this.bottomRight = newBounds.bottomRight;
    }

    /**
     * Resizes the rectangle to fit the 4 coordinate parameters
     * @param topX
     * @param topY
     * @param bottomX
     * @param bottomY
     */
    public void resizeTo(int topX, int topY, int bottomX, int bottomY) {
        this.topLeft.set(topX, topY);
        this.bottomRight.set(bottomX, bottomY);
    }

    /**
     * Method for checking collision with other Rectangle objects
     * @param r2 The other rectangle
     * @return True if the rectangles overlap somewhere
     */
    public boolean overlapsWith(Rectangle r2) {
        Rectangle r1 = this;

        if (r1.bottomRight.x < r2.topLeft.x
                || r1.bottomRight.y < r2.topLeft.y
                || r1.topLeft.x > r2.bottomRight.x
                || r1.topLeft.y > r2.bottomRight.y) {
            return false;
        }
        return true;
    }


}

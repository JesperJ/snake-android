package com.example.ultimatesnake.game.model;

import com.example.ultimatesnake.R;

/**
 * Enum to represent different food types
 */
public enum Pickup {
    FOOD,
    SUPER_FOOD,
    SLOW_POWER,
    DIET_PILL;

    public int sound;
    public void addSound(int sound) {
        this.sound = sound;
    }
}

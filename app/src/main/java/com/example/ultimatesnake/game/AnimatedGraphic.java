package com.example.ultimatesnake.game;

import android.os.Handler;

/**
 * Abstract class for graphical elements, holds methods for standard animations and properties for graphics.
 */
public abstract class AnimatedGraphic {
    public int color;

    public int alpha = 255;
    public boolean isFading = false;

    /**
     * Gradually reduces the alpha until the AnimatedGraphic is invisible
     * @param duration The duration to reduce the alpha over
     */
    public void fadeOut(int duration) {
        Handler handler = new Handler();
        int delay = (int) (1000 / (25.0 / duration));
        Runnable fade = new Runnable() {
            @Override
            public void run() {
                alpha -= 10;
                if (alpha < 10) alpha = 0;
                else handler.postDelayed(this, delay);
            }
        };
        handler.postDelayed(fade, delay);
        isFading = true;
    }

    public void setColor(int color) {
        this.color = color;
    }
    public int getColor() {
        return color;
    }

    public void setAlpha(int a) {
        alpha = a;
    }
    public int getAlpha() {
        return alpha;
    }
}

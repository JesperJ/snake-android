package com.example.ultimatesnake.game.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import androidx.core.content.res.ResourcesCompat;

import com.example.ultimatesnake.R;
import com.example.ultimatesnake.game.GameActivity;
import com.example.ultimatesnake.game.Rectangle;
import com.example.ultimatesnake.game.FloatingText;

import java.util.List;

/**
 * Renders a snake and potential food, power-ups and walls on a Canvas.
 * Uses GameActivity as a controller to read current game-status
 */
public class SnakeView extends View  {

    private final Paint painter = new Paint();
    private final Paint snakePainter = new Paint();

    private int levelWidth = 1;
    private int levelHeight = 1;

    private int viewWidth;
    private int viewHeight;

    private int difficulty;

    private GameActivity gameController;

    private ViewGrid grid = new ViewGrid(500, 500, 500, 500);

    public SnakeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {  // Med Edit Mode menas design-fliken i activity_game.xml, den fungerar inte annars
            this.gameController = (GameActivity) context;
            setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.moveColor, null));

            // Nedanstående måste göras i post - efter själva view:n skapats - eftersom t.ex annars ger getWidth() 0
            post(new Runnable() {
                @Override
                public void run() {

                    setLevelWidth(gameController.getLevelWidth());
                    setLevelHeight(gameController.getLevelHeight());

                    viewWidth = getWidth();
                    viewHeight = getHeight();
                    if ((float)viewHeight / (float)viewWidth < 1.77)
                        viewWidth = (int)(viewHeight / 1.77);

                    difficulty = gameController.getDifficulty();
                    getWalls();

                    grid = new ViewGrid(levelWidth, levelHeight, viewWidth, viewHeight);
                    setLayoutParams(new LinearLayout.LayoutParams(viewWidth, grid.getRequiredPixelHeight()));
                    invalidate();

                    snakePainter.setShader(gameController.getSnakeColor());
                }
            });
        }
    }
    public int getDrawHeight() {
        return grid.height();
    }
    private void setLevelWidth(int width) {
        this.levelWidth = width;
    }
    private void setLevelHeight(int height) {
        this.levelHeight = height;
    }
    private void getWalls() {
        walls = gameController.getWalls();
    }
    Rectangle renderRectangle = new Rectangle();
    List<Rectangle> foodList;
    List<Rectangle> snake;
    List<Rectangle> walls;
    List<Rectangle> powerups;
    List<FloatingText> texts;
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (!isInEditMode()) {

            painter.setColor(getResources().getColor(R.color.floorColor));
            canvas.drawRect(grid.getSquare(0, 0).top.x, grid.getSquare(0,0).top.y, grid.getSquare(199, 199).bottom.x, grid.getSquare(grid.width() - 1, grid.height() - 1).bottom.y, painter);

            // Rita ut mat
            foodList = gameController.getFood();
            painter.setColor(getResources().getColor(R.color.foodColor));
            for (Rectangle food : foodList){
                canvas.drawRect(grid.getSquare(food.topLeft.x, food.topLeft.y).top.x, grid.getSquare(food.topLeft.x, food.topLeft.y).top.y, grid.getSquare(food.bottomRight.x, food.bottomRight.y).bottom.x, grid.getSquare(food.bottomRight.x, food.bottomRight.y).bottom.y, painter);
            }

            // Rita ut powerups
            powerups = gameController.getPowerups();
            for (Rectangle powerup : powerups) {
                painter.setColor(powerup.getColor());
                painter.setAlpha(powerup.getAlpha());
                canvas.drawRect(grid.getSquare(powerup.topLeft.x, powerup.topLeft.y).top.x, grid.getSquare(powerup.topLeft.x, powerup.topLeft.y).top.y, grid.getSquare(powerup.bottomRight.x, powerup.bottomRight.y).bottom.x, grid.getSquare(powerup.bottomRight.x, powerup.bottomRight.y).bottom.y, painter);
            }


            // Rita ut väggar
            if(walls != null){
                painter.setColor(getResources().getColor(R.color.snakeColor));
                for(Rectangle wall : walls){
                    canvas.drawRect(grid.getSquare(wall.topLeft.x, wall.topLeft.y).top.x, grid.getSquare(wall.topLeft.x, wall.topLeft.y).top.y, grid.getSquare(wall.bottomRight.x, wall.bottomRight.y).bottom.x, grid.getSquare(wall.bottomRight.x, wall.bottomRight.y).bottom.y, painter);
                }
            }

            // Gå igenom varje del av ormen och rita ut den
            snake = gameController.getSnake();
            for (Rectangle segment : snake) {
                renderRectangle.resizeTo(0, 0, grid.width() - 1, grid.height() - 1);

                if (segment.overlapsWith(renderRectangle)) {
                    renderRectangle.topLeft.x = Math.max(segment.topLeft.x, 0);
                    renderRectangle.topLeft.y = Math.max(segment.topLeft.y, 0);
                    renderRectangle.bottomRight.x = Math.min(segment.bottomRight.x, grid.width() - 1);
                    renderRectangle.bottomRight.y = Math.min(segment.bottomRight.y, grid.height() - 1);
                    canvas.drawRect(grid.getSquare(renderRectangle.topLeft.x, renderRectangle.topLeft.y).top.x, grid.getSquare(renderRectangle.topLeft.x, renderRectangle.topLeft
                            .y).top.y, grid.getSquare(renderRectangle.bottomRight.x, renderRectangle.bottomRight.y).bottom.x, grid.getSquare(renderRectangle.bottomRight.x, renderRectangle.bottomRight.y).bottom.y, snakePainter);
                }

            }
            painter.setShader(null);
            // Rita ut texter
            texts = gameController.getFloatingTexts();
            for (FloatingText text : texts) {
                painter.setColor(text.getColor());
                painter.setAlpha(text.getAlpha());
                painter.setTextSize(text.getTextSize());

                float posX = (float) grid.getSquare(text.x, text.y).top.x;
                float posY = (float) grid.getSquare(text.x, text.y).top.y;

                canvas.drawText(text.getText(), posX, posY, painter);
            }
        }
    }
}

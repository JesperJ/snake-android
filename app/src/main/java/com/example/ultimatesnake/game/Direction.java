package com.example.ultimatesnake.game;

/**
 * An enum which represent different directions
 */
public enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
}

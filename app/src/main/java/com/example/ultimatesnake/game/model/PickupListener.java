package com.example.ultimatesnake.game.model;

public interface PickupListener {
    void call(Pickup pickup);
}

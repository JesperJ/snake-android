package com.example.ultimatesnake.game.model;

import android.graphics.Point;

import androidx.core.content.ContextCompat;

import com.example.ultimatesnake.ApplicationClass;
import com.example.ultimatesnake.R;
import com.example.ultimatesnake.game.Direction;
import com.example.ultimatesnake.game.FloatingText;
import com.example.ultimatesnake.game.FloatingTextManager;
import com.example.ultimatesnake.game.Rectangle;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

@SuppressWarnings("SpellCheckingInspection")
/**
 * Model which executes a game of Ultimate Snake. Once instantiated, SnakeGame will use the system time as reference for its updates. It will independant on how often it's called, the game speed can be altered with addSpeedMultiplier().
 */
public class SnakeGame {

    private int score;
    private boolean playing;

    private final int LEVEL_WIDTH;
    private final int LEVEL_HEIGHT;

    private int snakeWidth = 10;
    private int growthAmount;
    private final List<Rectangle> foodList = new ArrayList<>();
    private final List<Rectangle> wallList = new ArrayList<>();

    private final int FOOD_SIZE = 5;
    private final int MAX_FOOD = 3;
    private final int FOOD_RESPAWN_TIME = 3000;
    private final int foodScore = 50;

    private int difficulty;
    private PowerupManager powerupManager;
    private Snake snake;
    public List<Rectangle> getSnake() {
        return snake.getSegmentsOutlines();
    }

    private FloatingTextManager floatingTextManager = new FloatingTextManager();

    private Updater updater = new Updater();

    /**
     * Creates a snakegame with a specified difficulty level
     * @param difficulty to be played on
     */
    public SnakeGame(int difficulty, int width, int height){
        this.LEVEL_WIDTH = width;
        this.LEVEL_HEIGHT = height;
        score = 0;
        playing = true;
        growthAmount = snakeWidth + ((LEVEL_HEIGHT - 151) / 100);   // Scale up growth if the level height is big (for every 100 above 151, it will increase by 1)
        setSnakeSpeed(snakeSpeed - ((LEVEL_HEIGHT - 151) / 100));   // Scale up snake speed
        this.difficulty = difficulty;
        if (difficulty == 1) setSnakeSpeed(snakeSpeed - 2);
        else if (difficulty == 2) {
            setSnakeSpeed(snakeSpeed - 5);
            snakeWidth = 12;
            pressureSpeed -= 25;
        }
        snake = new Snake(LEVEL_WIDTH / 6, snakeWidth, new Point(LEVEL_WIDTH / 2 + 29, (LEVEL_HEIGHT / 3)), Direction.LEFT, new Rectangle(new Point(0,0), new Point(LEVEL_WIDTH, LEVEL_HEIGHT)));

        powerupManager = new PowerupManager();

        generateFood();
        initializeUpdater();
        initializeWalls(difficulty);
    }
    public List<Rectangle> getPowerups() {
        return powerupManager.getOutlines();
    }

    /**
     * Create a list with walls depending on the difficulty
     * @param difficulty to be played on
     */
    public void initializeWalls(int difficulty){
        if(difficulty == 1){
            wallList.add(new Rectangle(new Point(0,0), new Point(getLevelWidth() / 4, 4))); // Uppre vänstra hörnet horisontell
            wallList.add(new Rectangle(new Point(3 * (getLevelWidth() / 4), 0), new Point(getLevelWidth() - 1, 4))); // Uppre högra hörnet horisontell
            wallList.add(new Rectangle(new Point(0, 3 * getLevelHeight() / 4 - 1), new Point(3, getLevelHeight() - 1))); // Nedre vänstra hörnet vertikal
            wallList.add(new Rectangle(new Point(0, 0), new Point(3, getLevelHeight() / 4))); // Uppre vänstra hörnet vertikal
            wallList.add(new Rectangle(new Point(getLevelWidth() - 4, 0), new Point(getLevelWidth() - 1, getLevelHeight() / 4))); // Uppre högra hörnet vertikal
            wallList.add(new Rectangle(new Point(getLevelWidth() - 4, 3 * getLevelHeight() / 4 - 1), new Point(getLevelWidth() - 1, getLevelHeight() - 1))); // Nedre högra hörnet vertikal
            wallList.add(new Rectangle(new Point(0, getLevelHeight() - 4), new Point(getLevelWidth() / 4, getLevelHeight() - 1))); // Nedre vänstra hörnet horisontell
            wallList.add(new Rectangle(new Point(3 * getLevelWidth() / 4, getLevelHeight() - 4), new Point(getLevelWidth() - 1, getLevelHeight() - 1))); // Nedre högra hörnet horisontell
        } else if(difficulty == 2){
            wallList.add(new Rectangle(new Point(0, 0), new Point(getLevelWidth() - 1, 3)));
            wallList.add(new Rectangle(new Point(0, getLevelHeight() - 4), new Point(getLevelWidth() - 1, getLevelHeight() - 1)));
            wallList.add(new Rectangle(new Point(0, 0), new Point(2, getLevelHeight() - 1)));
            wallList.add(new Rectangle(new Point(getLevelWidth() - 3, 0), new Point(getLevelWidth() - 1, getLevelHeight() - 1)));
        }
    }

    /**
     * @return List<Rectangle> with walls on normal difficulty
     */
    public List<Rectangle> getWalls(){
        return wallList;
    }

    /**
     * Generates a rectangle at a random position on the level and adds it to the list of food
     */
    private void generateFood() {
        Rectangle food;
        int min = (int)(FOOD_SIZE*2.35);
        int maxX = LEVEL_WIDTH-1 - min;
        int maxY = LEVEL_HEIGHT-1 - min;
        while (true) {
            int foodPosX = (int)(Math.random() * (maxX - min)) + min;
            int foodPosY = (int)(Math.random() * (maxY - min)) + min;
            food = new Rectangle(new Point(foodPosX, foodPosY), new Point(foodPosX + FOOD_SIZE, foodPosY + FOOD_SIZE));
            if (!checkOverlap(food)) {
                break;
            }
        }
        foodList.add(food);
    }
    List<Double> multipliers = new ArrayList<>();

    /**
     * Puts a multiplier on the game's run speed. This can be used to slow down or speed up the game.
     * Calling this method multiple times will stack the effects multiplicatively. Adding 0.5 twice for example will result in the game running at 25% pace.
     * The spawn rate of pickups will NOT be affected.
     * After adding the multiplier, any FrequencyListeners to this game instance will be notified.
     *
     * @param multiplier A double. 0.5 will halve the game's run speed, 2.0 will double it, etc.
     */
    public void addSpeedMultiplier(Double multiplier) {
        multipliers.add(multiplier);
        refreshFrequency();
    }

    /**
     * Removes an active speed multiplier.
     * @param multiplier
     */
    public void removeSpeedMultiplier(Double multiplier) {
        multipliers.remove(multiplier);
        refreshFrequency();
    }

    /**
     * Needs to be called after a multiplier is added or removed to make their effects active.
     */
    private void refreshFrequency() {
        Double totalMultiplier = 1.0;
        for (Double multiplier : multipliers) {
            totalMultiplier = totalMultiplier * multiplier;
        }
        notifyFrequncyListeners(totalMultiplier);
        pressureScore.frequency = (int) (pressureSpeed / totalMultiplier);
        move.frequency = (int) (snakeSpeed / totalMultiplier);
    }
    private int snakeSpeed = 20; // How often the snake should move in miliseconds
    private BigDecimal speedMultiplier = BigDecimal.valueOf(1);
    private int growthSpeedMultiplier = 4;
    private int growthSpeed = snakeSpeed * growthSpeedMultiplier;
    private ScheduledUpdate move = new ScheduledUpdate((int) (snakeSpeed * speedMultiplier.doubleValue()), new Runnable(){
        @Override
        public void run() {
            snake.move();
            checkCollision();
        }
    });

    private int pressureSpeed = 250;
    /**
     * Counts down the score at a regular interval to pressure the player to continue eating
     * This gets updated in refreshFrequency(), so speed multipliers will affect it.
     */
    private ScheduledUpdate pressureScore = new ScheduledUpdate((int) (pressureSpeed * speedMultiplier.doubleValue()), new Runnable() {
        @Override
        public void run() {
            if (score > 1) score--;
        }
    });
    /**
     * Updates the model accordingly
     */
    private void initializeUpdater() {
        updater.scheduleForUpdate(new Runnable(){
            @Override
            public void run() {
                if (growthQueue > 0) {
                    snake.grow(1);
                    growthQueue--;
                }
            }
        }, growthSpeed);
        updater.scheduleForUpdate(move);
        updater.scheduleForUpdate(new Runnable(){
            @Override
            public void run() {
                if (foodList.size() < MAX_FOOD) {
                    generateFood();
                }
            }
        }, FOOD_RESPAWN_TIME);
        updater.scheduleForUpdate(new Runnable(){
            @Override
            public void run() {
                floatingTextManager.update();
            }
        }, 1000);
        updater.scheduleForUpdate(pressureScore);
    }
    public void setDirection(Direction dir) {
        snake.setDirection(dir);
    }

    /**
     * Moves the snake, checks for possible collisions with food / walls / itself
     */
    public void update() {
        updater.update();
    }

    /**
     * Checks if the snake collided with any food using Snake method checkCollision()
     * Increaces the score and grows the snake if true
     */
    private void checkFoodStatus() {

        for (Rectangle food : foodList) {
            if (snake.checkCollision(food)) {
                notifyPickupListeners(Pickup.FOOD);
                foodList.remove(food);
                growSnake(growthAmount);
                increaseScore(foodScore);
                break;
            }
       }
    }

    /**
     * Goes through all active powerups and checks if the snake's head overlaps with them. If a collision is found, the powerup gets consumed.
     */
    private void checkPowerupCollisions() {
        List<PowerupManager.Powerup> powerups = powerupManager.getSpawnedPowerups();
        for (PowerupManager.Powerup powerup : powerups) {
            if (snake.checkCollision(powerup.getOutline())) {
                powerupManager.consumePowerup(powerup);
                break;
            }
        }
    }
    private List<PickupListener> scoreListeners = new ArrayList<>();

    /**
     * Increases the player's score by the given amount and creates a FloatingText at the snake's head with the amount. The FloatingText fades away quickly.
     * @param amount The amount of score to be added and displayed
     */
    private void increaseScore(int amount) {
        score = score + amount;
        Point pos = new Point(snake.getHead().x, snake.getHead().y);
        Direction dir = snake.getDirection();
        if (dir == Direction.LEFT) pos.y += snakeWidth;
        else if (dir == Direction.UP) pos.x += snakeWidth;
        if (pos.x > LEVEL_WIDTH - snakeWidth) pos.x = LEVEL_WIDTH - snakeWidth;
        else if (pos.x < snakeWidth) pos.x = snakeWidth;
        if (pos.y > LEVEL_HEIGHT - snakeWidth) pos.y = LEVEL_HEIGHT - snakeWidth;
        else if (pos.y < snakeWidth) pos.y = snakeWidth;

        if (amount > foodScore) floatingTextManager.addFloatingText("+" + amount, pos, ContextCompat.getColor(ApplicationClass.getContext(), R.color.floatingText), floatingTextManager.defaultSize * 1.5f);
        else floatingTextManager.addFloatingText("+" + amount, pos, ContextCompat.getColor(ApplicationClass.getContext(), R.color.floatingText), floatingTextManager.defaultSize);
    }

    /**
     * Adds a PickupListener to the game. The listener will be called when the snake collides with score or other pickups.
     * @param listener The PickupListener to add
     */
    public void addPickupListener(PickupListener listener) {
        scoreListeners.add(listener);
    }

    /**
     * Calls all PickupListeners
     * @param pickup The pickup they should be notified of
     */
    private void notifyPickupListeners(Pickup pickup) {
        for (PickupListener listener : scoreListeners) {
            listener.call(pickup);
        }
    }

    boolean checkOverlap(Rectangle entity) {
        List<Rectangle> levelEntities = new ArrayList<>();
        levelEntities.addAll(wallList);
        levelEntities.addAll(snake.getSegmentsOutlines());
        levelEntities.addAll(foodList);
        levelEntities.addAll(powerupManager.getOutlines());

        for (Rectangle levelEntity : levelEntities) {
            if (levelEntity.overlapsWith(entity)) {
                return true;
            }
        }
        return false;
    }
    /**
     * Checks if the snake collided with either its self or the walls depending
     * on the difficulty.
     * If a collision has been found, isPlaying() will return false afterward
     */
    private void checkCollision(){
        checkFoodStatus();
        checkPowerupCollisions();
        for (int i = 1; i < snake.getSegmentsOutlines().size(); i++){
            if(snake.checkCollision(snake.getSegmentsOutlines().get(i))) {
                playing = false;
            }
        }
        if(difficulty == 1){
            for(int i = 0; i < getWalls().size(); i++){
                if(snake.checkCollision(getWalls().get(i))){
                    playing = false;
                }
            }
        } else if (difficulty == 2){
            for(int i = 0; i < getWalls().size(); i++) {
                if (snake.checkCollision(getWalls().get(i))) {
                    playing = false;
                }
            }
        }
    }

    /**
     * Refreshes the stored system time for the game's updater. This should be used when resuming the game after a pause.
     */
    public void resetTime() {
        updater.resetSystemTime();
    }

    /**
     * @return playing indication if the snake collided or not
     */
    public boolean isPlaying(){
        return playing;
    }

    private int growthQueue = 0;
    /**
     * Adds the given amount to the growth queue. The snake's tail will be extended gradually until the amount is depleted.
     * @param amountToGrow
     */
    private void growSnake(int amountToGrow) {
        growthQueue += amountToGrow;
    }

    /**
     * @return List<Rectangle> a list of all food currently in the game
     */
    public List<Rectangle> getFoods() {
        return foodList;
    }

    /**
     * @return the current direction of the snake
     */
    public Direction getDirection() {
        return snake.getDirection();
    }

    /**
     * @return the current score
     */
    public int getScore() {
        return score;
    }
    public int getSnakeSpeed() {
        return snakeSpeed;
    }

    /**
     * Sets the snake's speed to the given value
     * @param newSpeed The new speed, must be at least 0, otherwise nothing will happen.
     */
    public void setSnakeSpeed(int newSpeed) {
        if (newSpeed >= 0) {
            snakeSpeed = newSpeed;
            growthSpeed = snakeSpeed * growthSpeedMultiplier;
            refreshFrequency();
        }
    }

    /**
     * @return The width of the level in in-game units
     */
    public int getLevelWidth() {
        return LEVEL_WIDTH;
    }

    /**
     * @return The height of the level in in-game units
     */
    public int getLevelHeight() {
        return LEVEL_HEIGHT;
    }

    /**
     * @return A list of active FloatingText instances
     */
    public List<FloatingText> getFloatingTexts() {
        return floatingTextManager.getFloatingTexts();
    }

    /**
     * PowerupManager handles spawning and despawning of powerups. It contains local classes for the powerups which has their properties and effects. Use consumePowerup() when a powerup should be consumed.
     */
    class PowerupManager {

        List<Powerup> activePowerups = new ArrayList<>();
        List<Powerup> spawnedPowerups = new ArrayList<>();

        public PowerupManager () {

            updater.scheduleForUpdate(new Runnable() {
                @Override
                public void run() {
                    countdownPowerups();
                }
            }, 1000);
            updater.scheduleForUpdate(new Runnable() {
                @Override
                public void run() {
                    rollNewPowerup();
                }
            }, 4000);
        }

        private void rollNewPowerup() {
            Random rand = new Random();
            int roll = rand.nextInt(10);
            if (roll > 6) {
                Rectangle outline;
                while (true) {
                    roll = rand.nextInt(2);
                    int outlineX = rand.nextInt(getLevelWidth()- 8);
                    int outlineY = rand.nextInt(getLevelHeight() - 8);
                    outline = new Rectangle(new Point(outlineX, outlineY), new Point(outlineX + 8, outlineY + 8));
                    if (!checkOverlap(outline)) {
                        break;
                    }
                }
                roll = rand.nextInt(3);
                if (roll == 0) spawnedPowerups.add(new SlowPowerup(6, outline));
                else if (roll == 1) spawnedPowerups.add(new SuperFood(0, outline));
                else if (roll == 2) spawnedPowerups.add(new DietPill(3, outline));
            }
        }
        private void countdownPowerups() {
            Iterator<Powerup> i = activePowerups.iterator();
            while (i.hasNext()) {
                Powerup powerup = i.next();
                powerup.duration--;
                if (powerup.duration <= 0) {
                    powerup.deactivate();
                    i.remove();
                }
            }
            i = spawnedPowerups.iterator();
            while (i.hasNext()) {
                Powerup powerup = i.next();
                powerup.spawnedTime++;
                if (powerup.spawnedTime >= powerup.maxSpawnTime) {
                    i.remove();
                }
                else if (powerup.maxSpawnTime - powerup.spawnedTime == 4) {
                    powerup.outline.fadeOut(4);
                }
            }
        }

        /**
         * Activates the given powerup's effects and despawns it. SnakeGame's PickupListeners will be notified of the consumed powerup.
         * @param powerup
         */
        public void consumePowerup(Powerup powerup) {
            powerup.activate();
            activePowerups.add(powerup);
            spawnedPowerups.remove(powerup);
        }

        /**
         * @return List of powerups that are currently spawned (on the game board)
         */
        public List<Powerup> getSpawnedPowerups () {
            return spawnedPowerups;
        }

        /**
         * @return A list containing all the spawned powerups as Rectangle objects
         */
        public List<Rectangle> getOutlines() {
            List<Rectangle> outlines = new ArrayList<>();
            for (Powerup powerup : spawnedPowerups) {
                outlines.add(powerup.getOutline());
            }
            return outlines;
        }


        abstract class Powerup {
            int spawnedTime = 0;
            final int maxSpawnTime;
            long duration;
            Rectangle outline;

            Powerup(long duration, Rectangle outline, int maxSpawnTime, int color) {
                this.duration = duration;
                this.outline = outline;
                this.maxSpawnTime = maxSpawnTime;
                outline.color = color;
            }

            Rectangle getOutline() {
                return outline;
            }

            abstract void activate();
            abstract void deactivate();
        }
        class SlowPowerup extends Powerup {
            SlowPowerup(long duration, Rectangle outline) {
                super(duration, outline, 9, ContextCompat.getColor(ApplicationClass.getContext(), R.color.powerup_slow));
            }
            @Override
            void activate() {
                notifyPickupListeners(Pickup.SLOW_POWER);
                addSpeedMultiplier(0.6);
            }
            @Override
            void deactivate() {
                removeSpeedMultiplier(0.6);
            }
        }
        class SuperFood extends Powerup {
            SuperFood(long duration, Rectangle outline) {
                super(duration,outline, 7, ContextCompat.getColor(ApplicationClass.getContext(), R.color.powerup_superFood));
            }

            @Override
            void activate() {
                notifyPickupListeners(Pickup.SUPER_FOOD);
                increaseScore(150);
                growSnake(growthAmount);
            }

            @Override
            void deactivate() {
            }
        }
        class DietPill extends Powerup {
            ScheduledUpdate shrinker;
            int remainingShrink = (int)(snakeWidth * 2.5);
            DietPill(long duration, Rectangle outline) {
                super(duration, outline,9, ContextCompat.getColor(ApplicationClass.getContext(), R.color.powerup_dietPill));
                outline.bottomRight = new Point(outline.bottomRight.x, outline.bottomRight.y - (outline.bottomRight.y - outline.topLeft.y) / 4);
            }

            @Override
            void activate() {
                notifyPickupListeners(Pickup.DIET_PILL);
                addSpeedMultiplier(1.33);
                Runnable shrink = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (remainingShrink > 0) {
                                snake.shrink(1);
                                remainingShrink--;
                            }
                        } catch (IllegalStateException e) {}
                    }
                };
                shrinker = new ScheduledUpdate(growthSpeed / 2, shrink);
                updater.scheduleForUpdate(shrinker);
            }

            @Override
            void deactivate() {
                removeSpeedMultiplier(1.33);
                updater.cancelSchedule(shrinker);
            }
        }
    }
    public interface FrequencyListener {
        void onMultiplierChange(double frequency);
    }
    private List<FrequencyListener> frequencyListeners = new ArrayList<>();
    public void addFrequencyListener(FrequencyListener listener) {
        frequencyListeners.add(listener);
    }
    private void notifyFrequncyListeners(double frequency) {
        for (FrequencyListener listener : frequencyListeners) {
            listener.onMultiplierChange(frequency);
        }
    }
}


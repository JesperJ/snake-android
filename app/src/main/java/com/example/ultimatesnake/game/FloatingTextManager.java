package com.example.ultimatesnake.game;

import android.graphics.Point;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A class which handles FloatingText instances using an array, counting down their timers and executing their animations.
 */
public class FloatingTextManager {
    List<FloatingText> texts = new ArrayList<>();
    public float defaultSize = 64f;

    public void addFloatingText(String text, Point position, int color, float size) {
        texts.add(new FloatingText(text, position, 2, color, size));
    }

    /**
     * Needs to be called to update the animations and countdown the duration of FloatingTexts.
     */
    public void update() {
        Iterator<FloatingText> i = texts.iterator();
        while(i.hasNext()) {
            FloatingText text = i.next();
            text.duration--;
            if (text.duration <= 0) {
                i.remove();
            }
            if (text.duration <= 2 && !text.isFading) {
                text.fadeOut(1);
            }
        }
    }

    /**
     * @return This FloatingTextManager's list of FloatingText objects
     */
    public List<FloatingText> getFloatingTexts() {
        return texts;
    }
}

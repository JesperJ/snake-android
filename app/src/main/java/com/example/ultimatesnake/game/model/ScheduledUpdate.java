package com.example.ultimatesnake.game.model;

/**
 * ScheduledUpdate is designed to give the illusion of running a Runnable at a fixed frequency. It will actually run the Runnable multiple times to compensate for the time since its execute() method was called, which is why that time needs to be provided as a parameter in the method.
 */
public class ScheduledUpdate {
    public int frequency;
    public int frequencyOverlap = 0;
    Runnable task;
    public ScheduledUpdate(int frequency, Runnable task) {
        this.frequency = frequency;
        this.task = task;
    }

    public void execute(long timeSinceLastUpdate) {
        frequencyOverlap += timeSinceLastUpdate;
        while (frequencyOverlap >= frequency) {
            task.run();
            frequencyOverlap -= frequency;
        }
    }
}
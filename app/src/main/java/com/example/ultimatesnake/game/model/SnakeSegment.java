package com.example.ultimatesnake.game.model;

import android.graphics.Point;

import com.example.ultimatesnake.game.Direction;
import com.example.ultimatesnake.game.Rectangle;

@SuppressWarnings("SpellCheckingInspection")
/**
 * A SnakeSegment is treated like a Rectangle.
 * Contains methods to easily grow or sink a segment
 * and to find its location in the event of a rotaion
 */
public class SnakeSegment {

    private int length;
    private int width;
    private Direction direction;
    private Point head;

    public SnakeSegment (int length, int width, Point head, Direction direction) {
        this.length = length;
        this.width = width;
        this.head = head;
        this.direction = direction;
    }

    public void setWidth(int newWidth) {
        width = newWidth;
    }
    public int getWidth() {
        return width;
    }
    public int getLength() {
        return length;
    }

    /**
     * @return The current direction of the SnakeSegment. The direction decides which ends will be affected by shrinkHead(), increaseLength(), and decreaseLength().
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * Extends the tail of the SnakeSegment
     * @param amount
     */
    public void increaseLength (int amount) {
        this.length += amount;
    }

    /**
     * Decreses the length of the SnakeSegment from the tail end. Use shrinkHead() to do it in the other direction.
     * @param amount
     */
    public void decreaseLength (int amount) {
        this.length -= amount;
    }

    /**
     * Reduces the length of the SnakeSegment by removing the amount from its head (the tail end will stay in the same position).
     * Useful when replacing this segment's head with the head of a new segment.
     * @param amount
     */
    public void shrinkHead(int amount) {
        moveHead(-amount);
        decreaseLength(amount);
    }

    /**
     * Changes the position of the SnakeSegment in its current direction by the given amount
     * @param amount
     */
    public void moveHead (int amount) {
        if (direction == Direction.UP)
            head.y -= amount;
        else if (direction == Direction.DOWN)
            head.y += amount;
        else if (direction == Direction.LEFT)
            head.x -= amount;
        else
            head.x += amount;
    }

    /**
     * @return  A point in the top of the segment. The point rotates naturally along with the segement's direction.
     *  UP -  The point is in the upper left corner.
     *  DOWN - The point is in the lower right corner
     *  RIGHT - The point is in the upper right corner.
     *  LEFT - The point is in the lower left corner.
     */
    public Point getHead() {
        return head;
    }
    /**
     * @return  A point where the head would be if the head changed direction.
     */
    public Point headWithChangedDirection(Direction dir) {
        /* Eftersom "huvudet" är en punkt i det vänstra hörnet av ormens ansikte måste
                det flyttas till att vara i rätt position när ormen vänder på sig
                Detta kräver separat logik i 8 olika fall
                 */
        Point newHead = null;
        if (dir == Direction.UP) {
            // Fall 1: Ormen rör sig åt höger och ska vända sig uppåt
            if (this.direction == Direction.RIGHT) {
                newHead = new Point(head.x - width + 1, head.y);
            }
            // Fall 2: Ormen rör sig åt vänster och ska vända sig uppåt
            else if (this.direction == Direction.LEFT) {
                newHead = new Point(head.x, head.y - width + 1);
            }
        } else if (dir == Direction.DOWN) {
            // Etc...
            if (this.direction == Direction.RIGHT) {
                newHead = new Point(head.x, head.y + width - 1);
            } else if (this.direction == Direction.LEFT) {
                newHead = new Point(head.x + width - 1, head.y);
            }
        } else if (dir == Direction.LEFT) {
            if (this.direction == Direction.UP) {
                newHead = new Point(head.x, head.y + width - 1);
            } else if (this.direction == Direction.DOWN) {
                newHead = new Point(head.x - width + 1, head.y);
            }
        } else if (dir == Direction.RIGHT) {
            if (this.direction == Direction.UP) {
                newHead = new Point(head.x + width - 1, head.y);
            } else if (this.direction == Direction.DOWN) {
                newHead = new Point(head.x, head.y - width + 1);
            }
        }
        return newHead;
    }

    /**
     * Uses the SnakeSegment's status to create a Rectangle-object to be used instead
     */
    public Rectangle getOutline() {
        int topX, topY, bottomX, bottomY;
        if (direction == Direction.UP) {
            topX = head.x;
            topY = head.y;
            bottomX = head.x + width - 1;
            bottomY = head.y + length - 1;
        }
        else if (direction == Direction.DOWN) {
            topX = head.x - width + 1;
            topY = head.y - length + 1;
            bottomX = head.x;
            bottomY = head.y;
        }
        else if (direction == Direction.LEFT) {
            topX = head.x;
            topY = head.y - width + 1;
            bottomX = head.x + length - 1;
            bottomY = head.y;
        }
        else {
            topX = head.x - length + 1;
            topY = head.y;
            bottomX = head.x;
            bottomY = head.y + width - 1;
        }
        return new Rectangle(new Point(topX, topY), new Point(bottomX, bottomY));
    }

}

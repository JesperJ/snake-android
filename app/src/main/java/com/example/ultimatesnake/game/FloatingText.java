package com.example.ultimatesnake.game;

import android.graphics.Point;

import com.example.ultimatesnake.game.AnimatedGraphic;

/**
 * A class which represent a floating textview
 */
public class FloatingText extends AnimatedGraphic {
    private String text;
    private Point position;
    public int x;
    public int y;
    private float textSize;
    public int duration;

    public FloatingText(String text, Point position, int color, float textSize) {
        this.text = text;
        this.position = position;
        this.color = color;
        this.textSize = textSize;
        this.x = position.x;
        this.y = position.y;
    }
    public FloatingText(String text, Point position, int duration, int color, float textSize) {
        this(text, position, color, textSize);
        this.duration = duration;
    }
    public float getTextSize() {
        return textSize;
    }
    public String getText() {
        return text;
    }
    public Point getPosition() {
        return position;
    }
}

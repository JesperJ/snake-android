package com.example.ultimatesnake.game.model;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
/**
 * A class which handles updates, every update at a specified frequency gets added to this class
 * For example, the snakes movement at a certain frequency
 * The schedules updates in Update executes independently of how often method update() is called
 * So the frequency in GameActivity is independent from the updater and if the users phone starts lagging the game will still continue in the same frequency
 */
class Updater {
    private long lastUpdateTime;
    public List<ScheduledUpdate> schedule = new ArrayList<>();
    public Updater() {
        this.lastUpdateTime = System.currentTimeMillis();
    }

    /**
     * Executes all scheduled updates according to the system time.
     */
    public void update() {
        while (!scheduleQueue.isEmpty()) {
            schedule.add(scheduleQueue.poll());
        }
        long timeSinceLastUpdate = System.currentTimeMillis() - lastUpdateTime;
        this.lastUpdateTime = System.currentTimeMillis();
        for (ScheduledUpdate entry : schedule) {
            entry.execute(timeSinceLastUpdate);
        }
        while (!cancelQueue.isEmpty()) {
            schedule.remove(cancelQueue.poll());
        }
    }

    private Queue<ScheduledUpdate> scheduleQueue = new ArrayDeque<>();
    /**
     * @param task Code to execute
     * @param updateFrequency Frequency in milliseconds to run at
     */
    public void scheduleForUpdate(Runnable task, int updateFrequency) {
        scheduleQueue.offer(new ScheduledUpdate(updateFrequency, task));
    }

    private Queue<ScheduledUpdate> cancelQueue = new ArrayDeque<>();

    /**
     * @param update to remove from the update queue
     */
    public void cancelSchedule (ScheduledUpdate update) {
        cancelQueue.offer(update);
    }

    /**
     * @param update to add to the update queue
     */
    public void scheduleForUpdate(ScheduledUpdate update) {
        scheduleQueue.offer(update);
    }

    /**
     * This method should be called when resuming after a pause, otherwise scheduled updates will be executed as if there was never a pause.
     */
    public void resetSystemTime() {
        this.lastUpdateTime = System.currentTimeMillis();
    }
}

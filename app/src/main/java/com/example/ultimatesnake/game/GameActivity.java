package com.example.ultimatesnake.game;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Shader;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.ultimatesnake.GameEndedActivity;
import com.example.ultimatesnake.R;
import com.example.ultimatesnake.game.model.Pickup;
import com.example.ultimatesnake.game.model.PickupListener;
import com.example.ultimatesnake.game.model.SnakeGame;
import com.example.ultimatesnake.game.view.SnakeView;

import java.util.List;

/**
 * GameActivity acts like a controller between SnakeGame (model) and SnakeView (gui).
 * Updates the model at a specified frequency.
 * The activity is controlled through swipe gestures.
 */
public class GameActivity extends AppCompatActivity
implements GestureDetector.OnGestureListener {

    protected SnakeView gameView;
    protected SnakeGame game;

    Runnable runnable;
    Handler handler = new Handler();
    final int updateFrequency = 24;
    protected TextView score;
    private int difficulty;
    private int snakeColor;

    private MediaPlayer gameMusic;
    private SoundPool soundPool;
    private int deathSound;

    private GestureDetectorCompat swipeDetector;
    private SharedPreferences sp;
    private boolean playGameMusic;
    private boolean playSoundEffects;
    private boolean playAllMusic;

    /**
     * Creates the activity and sets listeners to respective gestures
     * Retrieves possible information regarding difficulty level and snake color
     * Creates an instance of the model (snake game)
     * Loads potential music and sound effects
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |  View.SYSTEM_UI_FLAG_FULLSCREEN);

        sp = getSharedPreferences("music",Activity.MODE_PRIVATE);
        playAllMusic = sp.getBoolean("musicState", playGameMusic);
        playGameMusic = sp.getBoolean("gameMusicState", playGameMusic);
        playSoundEffects = sp.getBoolean("soundEffectsState", playSoundEffects);

        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |  View.SYSTEM_UI_FLAG_FULLSCREEN);
        difficulty = getIntent().getIntExtra("difficulty", 0);
        setContentView(R.layout.activity_game);

        sp = getSharedPreferences("snakeColor", Activity.MODE_PRIVATE);
        snakeColor = sp.getInt("snakeColor", 0);

        game = new SnakeGame(difficulty, 200, 345);
        game.addPickupListener(scoreListener);

        gameView = findViewById(R.id.game_canvas);

        score = findViewById(R.id.score);
        score.setText("SCORE : " + game.getScore());

        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        soundPool = new SoundPool.Builder().setAudioAttributes(attributes).build();
        Pickup.FOOD.addSound(soundPool.load(this, R.raw.eatsound, 1));
        Pickup.SUPER_FOOD.addSound(soundPool.load(this, R.raw.superfood, 1));
        Pickup.DIET_PILL.addSound(soundPool.load(this, R.raw.dietpill, 1));
        Pickup.SLOW_POWER.addSound(soundPool.load(this, R.raw.slowpower, 1));
        deathSound = soundPool.load(this, R.raw.gameoversound, 1);
        if(snakeColor == 2) {
            gameMusic = MediaPlayer.create(this, R.raw.greenanaconda);
        } else if (snakeColor == 4) {
            gameMusic = MediaPlayer.create(this, R.raw.pinkfloyd);
        }
        else {
            gameMusic = MediaPlayer.create(this, R.raw.runamok);
        }
        music();
        game.addFrequencyListener(new SnakeGame.FrequencyListener() {
            @Override
            public void onMultiplierChange(double frequency) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(gameMusic.isPlaying())
                        gameMusic.setPlaybackParams(gameMusic.getPlaybackParams().setSpeed((float)frequency));
                }
            }
        });
        swipeDetector = new GestureDetectorCompat(this, this);
        findViewById(R.id.activity_layout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return swipeDetector.onTouchEvent(motionEvent);
            }
        });

        loadShaders();
    }

    private BitmapShader pinkShader;
    private BitmapShader blueShader;
    private BitmapShader greenShader;
    private BitmapShader redShader;
    private BitmapShader cosmosShader;
    /**
     * Loads the snake shader corresponding to snakeColor.
     */
    private void loadShaders() {
        Bitmap bitmap;
        if (snakeColor == 0) {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.pinkshader);
            pinkShader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        }
        else if (snakeColor == 1) {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.redshader);
            redShader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        }
        else if (snakeColor == 2) {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.greenshader);
            greenShader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        }
        else if (snakeColor == 3) {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.blueshader);
            blueShader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        }
        else if (snakeColor == 4) {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cosmos);
            cosmosShader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        ViewGroup view = findViewById(R.id.activity_layout);
        view.post(new Runnable() {
            @Override
            public void run() {
                float height = view.getHeight();
                float width = view.getWidth();
                Log.d("Ratio", height + "");
                Log.d("Ratio", "Height/width: " + height/width);
                if (height / width <= 1.77) {
                    Log.d("Ratio", "Returns true");
                    view.removeView(findViewById(R.id.bottom_panel));
                    findViewById(R.id.score_panel).setLayoutParams(new LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, 0, 4.0f));

                }
            }
        });
    }

    /**
     * @return a list of Rectangle objects from the game instance, representing the whole snake
     */
    public List<Rectangle> getSnake() {
        return game.getSnake();
    }

    private void music(){
        if(!(playGameMusic || playAllMusic)){
            gameMusic.start();
            gameMusic.setLooping(true);
        } else {
            gameMusic.stop();
        }
    }

    @Override
    protected void onResume() {
        game.resetTime();
        gameMusic.start();
        super.onResume();
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {
                updateModel();
                gameView.invalidate();
                handler.postDelayed(this, updateFrequency);
            }
        }, updateFrequency);
        music();
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable);
        gameMusic.pause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        gameMusic.pause();
    }



    /**
     * Checks what color was selected in the main menu
     * @return Shader representing a specific color from Color resources
     */
    public Shader getSnakeColor() {
        snakeColor = sp.getInt("snakeColor", 0);
        Log.w("colorsnake", String.valueOf(snakeColor));
        if(snakeColor == 0){
            return pinkShader;
        } else if(snakeColor == 1){
            return redShader;
        } else if (snakeColor == 2){
            return greenShader;
        } else if (snakeColor == 3){
            return blueShader;
        } else {
            return cosmosShader;
        }
    }

    /**
     * @return The height of the game's level in in-game units (not pixels!)
     */
    public int getLevelHeight() { return game.getLevelHeight();}

    /**
     * @return The width of the game's level in in-game units (not pixels!)
     */
    public int getLevelWidth() {
        return game.getLevelWidth();
    }

    /**
     * @return A list of all food currently spawned in the game, represented as Rectangle objects
     */
    public List<Rectangle> getFood() { return game.getFoods(); }

    /**
     * @return A list of Rectangle objects for the level's walls
     */
    public List<Rectangle> getWalls(){
        return game.getWalls();
    }

    /**
     * @return A list of Rectangle objects for all powerups currently spawned in the game
     */
    public List<Rectangle> getPowerups() {
        return game.getPowerups();
    }

    /**
     * @return The selected game difficulty
     */
    public int getDifficulty(){ return difficulty; }

    /**
     * @return A list of all FloatingText instances currently active in the game. If the game has created a FloatingText it is intended to be drawn.
     */
    public List<FloatingText> getFloatingTexts() {
        return game.getFloatingTexts();
    }

    /**
     * Updates the model, the score on the top panel, and checks for death
     */
    private void updateModel() {
        game.update();
        score.setText("SCORE : " + game.getScore());

        if (!game.isPlaying()) {
            gameMusic.stop();
            if(!(playSoundEffects || playAllMusic)){
                soundPool.play(deathSound, 1, 1, 0, 0, 1);
            }
            Intent gameEnd = new Intent(GameActivity.this, GameEndedActivity.class);
            gameEnd.putExtra("score", game.getScore());
            gameEnd.putExtra("difficulty", getDifficulty());
            startActivity(gameEnd);
        }
    }

    private final PickupListener scoreListener = new PickupListener() {
        @Override
        public void call(Pickup pickup) {
          if(!(playSoundEffects || playAllMusic))
            soundPool.play(pickup.sound, 1, 1, 0, 0, 1);
        }
    };

    @Override
    public void onBackPressed(){
        gameMusic.stop();
        super.onBackPressed();
    }

    @Override
    public boolean onDown(@NonNull MotionEvent motionEvent) {
        return true;
    }

    @Override
    public void onShowPress(@NonNull MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(@NonNull MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(@NonNull MotionEvent motionEvent1, @NonNull MotionEvent motionEvent2, float distanceX, float distanceY) {
        distanceX = Math.abs(distanceX);
        distanceY = Math.abs(distanceY);
        if (distanceY > distanceX) {
            if (motionEvent1.getRawY() < motionEvent2.getRawY()) {
                game.setDirection(Direction.DOWN);
            }
            else {
                game.setDirection(Direction.UP);
            }
        }
        else if (distanceX > distanceY) {
            if (motionEvent1.getRawX() < motionEvent2.getRawX()) {
                game.setDirection(Direction.RIGHT);
            }
            else {
                game.setDirection(Direction.LEFT);
            }
        }
        return true;
    }

    @Override
    public void onLongPress(@NonNull MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(@NonNull MotionEvent motionEvent, @NonNull MotionEvent motionEvent1, float v, float v1) {
        return false;
    }
}

package com.example.ultimatesnake.game;

public interface StopGameInterface {
    void stopGame();
    void resumeGame();
}

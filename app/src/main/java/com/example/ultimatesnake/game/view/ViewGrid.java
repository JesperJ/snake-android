package com.example.ultimatesnake.game.view;

import android.graphics.Point;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("SpellCheckingInspection")

/**
 * Represents a grid over a specified width and height.
 * ViewGrid divides the pixels evenly according to height and width.
 * The remaining pixels gets smushed evenly over the grid.
 * Some squares becomes some pixels larger, but at high resolutions there is no visual difference
 */
public class ViewGrid {
    private int xSquares;
    private int ySquares;

    private BigDecimal gridWidth;
    private BigDecimal gridHeight;

    private BigDecimal abstractPixelSize;

    private List<List<Square>> gridX;

    private int requiredPixelHeight;

    public ViewGrid (int horizontalSquares, int verticalSquares, int pixelWidth, int pixelHeight) {
        this.xSquares = horizontalSquares;
        this.ySquares = verticalSquares;
        this.gridWidth = BigDecimal.valueOf(pixelWidth);
        this.gridHeight = BigDecimal.valueOf(pixelHeight);

        abstractPixelSize = gridWidth.divide(BigDecimal.valueOf(xSquares), 3, RoundingMode.HALF_UP);

        System.out.println(abstractPixelSize);
        buildGrid();
    }
    private void buildGrid() {
        gridX = new ArrayList<>();

        BigDecimal pixelOverlapX = BigDecimal.valueOf(0);
        BigDecimal pixelOverlapY = BigDecimal.valueOf(0);

        int standardSize = abstractPixelSize.intValue();
        BigDecimal leftOverPixel = abstractPixelSize.remainder(new BigDecimal(1));

        int movedX = 0;
        int movedY = 0;

        for (int x = 0; x < xSquares; x++) {
            movedY = 0;
            pixelOverlapY = BigDecimal.valueOf(0);
            List<Square> gridY = new ArrayList<>();

            pixelOverlapX = pixelOverlapX.add(leftOverPixel);
            int currentOverlapX = pixelOverlapX.intValue();
            int squareSizeX = standardSize;
            for (int i = 0; i < currentOverlapX; i++) {
                squareSizeX++;
                pixelOverlapX = pixelOverlapX.subtract(BigDecimal.valueOf(1));
            }
            for (int y = 0; y < ySquares; y++) {
                int currentOverlapY = pixelOverlapY.intValue();
                int squareSizeY = standardSize;
                for (int i = 0; i < currentOverlapY; i++) {
                    squareSizeY++;
                    pixelOverlapY = pixelOverlapY.subtract(BigDecimal.valueOf(1));
                }
                gridY.add(new Square(new Point(movedX, movedY), new Point(movedX + squareSizeX, movedY + squareSizeY)));
                movedY += squareSizeY;
            }
            gridX.add(gridY);
            movedX += squareSizeX;
        }

        requiredPixelHeight = movedY;
    }
    public Square getSquare(int x, int y) {
        return gridX.get(x).get(y);
    }

    public int width() {
        return this.xSquares;
    }
    public int height() {
        return this.ySquares;
    }
    public int getRequiredPixelHeight() {return requiredPixelHeight;}
    public class Square {
        public Point top;      // Vänstra hörnet högst upp
        public Point bottom;   // Högra hörnet längst ner

        public Square(Point top, Point bottom) {
            this.top = top;
            this.bottom = bottom;
        }
    }



}

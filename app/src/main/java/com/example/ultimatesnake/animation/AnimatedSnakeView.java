package com.example.ultimatesnake.animation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import androidx.core.content.res.ResourcesCompat;

import com.example.ultimatesnake.MainActivity;
import com.example.ultimatesnake.R;
import com.example.ultimatesnake.game.view.ViewGrid;
import com.example.ultimatesnake.game.Rectangle;

import java.util.List;

/**
 * A class which renders the game on a canvas, using data from MainActivity class
 */
public class AnimatedSnakeView extends View {

    private final Paint painter = new Paint();

    private int levelWidth = 1;
    private int levelHeight = 1;

    private int viewWidth;
    private int viewHeight;

    private MainActivity gameController;
    private ViewGrid grid = new ViewGrid(500, 500, 500, 500);

    public AnimatedSnakeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {  // Med Edit Mode menas design-fliken i activity_game.xml, den fungerar inte annars
            this.gameController = (MainActivity) context;
            setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorBackground, null));

            // Nedanstående måste göras i post - efter själva view:n skapats - eftersom t.ex annars ger getWidth() 0
            post(new Runnable() {
                @Override
                public void run() {

                    setLevelWidth(gameController.getLevelWidth());
                    setLevelHeight(gameController.getLevelHeight());

                    viewWidth = getWidth();
                    viewHeight = getHeight();

                    grid = new ViewGrid(levelWidth, levelHeight, viewWidth, viewHeight);
                    setLayoutParams(new LinearLayout.LayoutParams(viewWidth, grid.getRequiredPixelHeight()));

                    invalidate();
                }
            });
        }
    }

    private void setLevelWidth(int width) {
        this.levelWidth = width;
    }
    private void setLevelHeight(int height) {
        this.levelHeight = height;
    }

    Rectangle renderRectangle = new Rectangle();

    /**
     * Paints the snake and the food on the canvas
     * @param canvas
     */
    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cosmos);
    BitmapShader cosmosShader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (!isInEditMode()) {

            painter.setColor(getResources().getColor(R.color.floorColor));
            canvas.drawRect(grid.getSquare(0, 0).top.x, grid.getSquare(0,0).top.y, grid.getSquare(199, 199).bottom.x, grid.getSquare(grid.width() - 1, grid.height() - 1).bottom.y, painter);

            List<Rectangle> snake = gameController.getSnake();
            List<Rectangle> foodList = gameController.getFood();

            painter.setColor(getResources().getColor(R.color.foodColor));
            for (Rectangle food : foodList){
                canvas.drawRect(grid.getSquare(food.topLeft.x, food.topLeft.y).top.x, grid.getSquare(food.topLeft.x, food.topLeft.y).top.y, grid.getSquare(food.bottomRight.x, food.bottomRight.y).bottom.x, grid.getSquare(food.bottomRight.x, food.bottomRight.y).bottom.y, painter);
            }

            // Gå igenom varje del av ormen och rita ut den
            int snakeColor = gameController.getSnakeColor();
            if (snakeColor < 4) {
                painter.setColor(gameController.getSnakeColor());
            } else {
                painter.setShader(cosmosShader);
            }
            for (Rectangle segment : snake) {
                renderRectangle.resizeTo(0, 0, grid.width() - 1, grid.height() - 1);
                if (segment.overlapsWith(renderRectangle)) {
                    renderRectangle.topLeft.x = Math.max(segment.topLeft.x, 0);
                    renderRectangle.topLeft.y = Math.max(segment.topLeft.y, 0);
                    renderRectangle.bottomRight.x = Math.min(segment.bottomRight.x, grid.width() - 1);
                    renderRectangle.bottomRight.y = Math.min(segment.bottomRight.y, grid.height() - 1);
                    canvas.drawRect(grid.getSquare(renderRectangle.topLeft.x, renderRectangle.topLeft.y).top.x, grid.getSquare(renderRectangle.topLeft.x, renderRectangle.topLeft
                            .y).top.y, grid.getSquare(renderRectangle.bottomRight.x, renderRectangle.bottomRight.y).bottom.x, grid.getSquare(renderRectangle.bottomRight.x, renderRectangle.bottomRight.y).bottom.y, painter);
                }
            }
            painter.setShader(null);
        }
    }
}

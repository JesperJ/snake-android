package com.example.ultimatesnake;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

import com.example.ultimatesnake.game.StopGameInterface;

/**
 * Fragment which displays three checkbox options to mute music and sound effects
 * Uses sharedpreferences to store the selected value
 */
public class OptionFragment extends Fragment {

    private CheckBox mainMusic;
    private CheckBox gameMusic;
    private CheckBox soundEffects;
    private boolean mainMusicIsPlaying;
    private boolean gameMusicIsOn;
    private boolean soundEffectsIsOn;
    private SharedPreferences musicPreferences;
    private StopGameInterface stopGameInterface;
    private Button back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        stopGameInterface = (StopGameInterface) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_option, container, false);
        if(view != null){
            mainMusic  = view.findViewById(R.id.mainMusicCheckBox);
            musicPreferences = getActivity().getSharedPreferences("music",Activity.MODE_PRIVATE);
            mainMusicIsPlaying = musicPreferences.getBoolean("musicState",false);

            gameMusic = view.findViewById(R.id.gameMusicCheckBox);
            gameMusicIsOn = musicPreferences.getBoolean("gameMusicState", false);

            soundEffects = view.findViewById(R.id.soundEffectsCheckBox);
            soundEffectsIsOn = musicPreferences.getBoolean("soundEffectsState", false);

            back = view.findViewById(R.id.backButton);
            back.setOnClickListener(e -> backPressed());

            if(mainMusicIsPlaying) mainMusic.setChecked(true);
            else mainMusic.setChecked(false);

            if(gameMusicIsOn) gameMusic.setChecked(true);
            else gameMusic.setChecked(false);

            if(soundEffectsIsOn) soundEffects.setChecked(true);
            else soundEffects.setChecked(false);
        }
        return view;
    }

    private void backPressed() {
        getActivity().onBackPressed();
    }

    @Override
    public void onStart(){
        super.onStart();
        saveData();
        stopGameInterface.stopGame();
    }

    private void saveData(){
        mainMusicIsPlaying = mainMusic.isChecked();

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("music", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("musicState", mainMusicIsPlaying);

        gameMusicIsOn = gameMusic.isChecked();
        editor.putBoolean("gameMusicState", gameMusicIsOn);

        soundEffectsIsOn = soundEffects.isChecked();
        editor.putBoolean("soundEffectsState", soundEffectsIsOn);
        editor.apply();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        saveData();
        stopGameInterface.resumeGame();
    }

}
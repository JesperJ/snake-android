package com.example.ultimatesnake;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import static android.content.ContentValues.TAG;
import android.util.Log;
import android.widget.EditText;
import com.example.ultimatesnake.database.BubbleSorter;
import com.example.ultimatesnake.database.HighScore;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.example.ultimatesnake.game.GameActivity;

/**
 * Class which represents a gameover screen
 * Retrieves data from the database and displays the correct highscores depending on
 * difficulty level
 * Allows user to submit their score if they reached the currently top 5
 */
public class GameEndedActivity extends AppCompatActivity {

    private TextView score;
    private TextView topFive;
    private Button yes;
    private Button no;
    private Button submit;

    private TextView outputName;
    private TextView outputHighScore;
    private EditText newHighScoreName;

    private String topHighScores;
    private String topNames;
    private String highScoreName;

    private int potentialHighScore;
    private boolean lowScoreCheck;
    private List<HighScore> currentValues;

    private int difficulty;
    private String difficultyString;

    private FirebaseDatabase database = FirebaseDatabase.getInstance("https://highscorertdb-default-rtdb.europe-west1.firebasedatabase.app/");
    private DatabaseReference databaseHighScoreReference;


    /**
     * Creates the GameEndedActivity which presents the final result of the game
     * to the user with different options and result prompts depending on the
     * difficulty level
     * If the user's score is higher than the current 5th highscore, the user can enter its
     * initials to submit their score
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_ended);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        difficulty = getIntent().getIntExtra("difficulty", 0);
        if(difficulty == 1){
            databaseHighScoreReference = database.getReference("NormalHighscore");
            difficultyString = "Normal mode";
        } else if(difficulty == 2){
            databaseHighScoreReference = database.getReference("HardHighscore");
            difficultyString = "Hard mode";
        } else {
            databaseHighScoreReference = database.getReference("EasyHighscore");
            difficultyString = "Child's play";
        }

        score = findViewById(R.id.scoreField);
        score.setText("You scored " + getIntent().getIntExtra("score", 0) + " points on " + difficultyString);
        topFive = findViewById(R.id.topPlayerField);
        topFive.setText(difficultyString + " highscore");
        yes = findViewById(R.id.yesButton);
        no = findViewById(R.id.noButton);
        submit = findViewById(R.id.submitButton);

        yes.setOnClickListener(e -> startNewGame());
        no.setOnClickListener(e -> exitGame());

        outputName = findViewById(R.id.playerNameFormDatabase);
        outputHighScore = findViewById(R.id.playerScoreFromDatabase);
        newHighScoreName = findViewById(R.id.playerNameToSubmit);
        newHighScoreName.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(3)});

        databaseHighScoreReference.addValueEventListener(new ValueEventListener() {

            /**
             * Retrives the current top 5 higscores (A name and score) from a database
             * which gets added to a string which is displayed
             * @param dataSnapshot
             */
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    currentValues = new ArrayList<>();
                    for (DataSnapshot value : dataSnapshot.getChildren()) {
                        currentValues.add(value.getValue(HighScore.class));
                    }
                    topHighScores = "";
                    topNames = "";
                    int counter = 1;
                    for (HighScore i : currentValues) {
                        topHighScores += i.getHighScore() + "\n";
                        topNames = topNames + counter + " " + i.getName() + "\n";
                        counter++;
                    }
                    outputName.setText(topNames);
                    outputHighScore.setText(topHighScores);
                    if(currentValues.get(4).getHighScore() < getIntent().getIntExtra("score", 0)){
                        submit.setOnClickListener(e -> sendData());
                    }else{
                        newHighScoreName.setVisibility(View.GONE);
                        submit.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

    }

    /**
     * Retrives the value of the textfield which the user entered their initials and
     * with the score, sort the new highscores with the current highscores using bubblesort
     * and add the new list to the database
     */
    private void sendData() {
        highScoreName = newHighScoreName.getText().toString();
        if(highScoreName.isEmpty()){
            return;
        }
        submit.setEnabled(false);
        BubbleSorter<HighScore> bubbleSorter = new BubbleSorter<>();
        potentialHighScore = getIntent().getIntExtra("score", 0);
        HighScore newHighScore = new HighScore(highScoreName, potentialHighScore);
        List<HighScore> defaultValues = new ArrayList<>();
        defaultValues.add(new HighScore(" ", 0));
        defaultValues.add(new HighScore(" ", 0));
        defaultValues.add(new HighScore(" ", 0));
        defaultValues.add(new HighScore(" ", 0));
        defaultValues.add(new HighScore(" ", 0));
        bubbleSorter.sort(defaultValues);
        Collections.reverse(defaultValues);

        if (currentValues == null) {
            databaseHighScoreReference.setValue(defaultValues);
        } else {
            defaultValues.clear();
            for (HighScore i : currentValues) {
                defaultValues.add(i);
            }

            for (int i = 0; i < defaultValues.size(); i++) {
                if (defaultValues.get(i).getHighScore() < newHighScore.getHighScore() && !highScoreName.isEmpty()) {
                    defaultValues.add(newHighScore);
                    lowScoreCheck = false;
                    break;
                } else {
                    lowScoreCheck = true;
                }
            }

            bubbleSorter.sort(defaultValues);
            Collections.reverse(defaultValues);
            if (lowScoreCheck == false) {
                defaultValues.remove(defaultValues.size() - 1);
            }
            databaseHighScoreReference.setValue(defaultValues);
        }
    }

    /**
     * Listener to the button 'no', starts the MainActivity
     */
    private void exitGame() {
        Intent mainActivity = new Intent(this, MainActivity.class);
        mainActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainActivity);
    }

    /**
     * Listener to the button 'yes' and starts the GameActivity again with the same
     * difficulty level and snake color as recently played
     */
    private void startNewGame() {
        Intent startOver = new Intent(this, GameActivity.class);
        startOver.putExtra("difficulty", getIntent().getIntExtra("difficulty", 0));
        startActivity(startOver);
    }

    /**
     * Returns the user to the main-menu if the back-button is pressed
     */
    @Override
    public void onBackPressed(){
        exitGame();
    }
}
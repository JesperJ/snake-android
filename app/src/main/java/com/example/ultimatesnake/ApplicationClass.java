package com.example.ultimatesnake;

import android.app.Application;
import android.content.Context;

public class ApplicationClass extends Application {

    public static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
    }
    public static Context getContext() {
        return context;
    }
}

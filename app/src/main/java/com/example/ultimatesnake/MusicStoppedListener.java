package com.example.ultimatesnake;

public interface MusicStoppedListener {
    void onMusicStopped();
}

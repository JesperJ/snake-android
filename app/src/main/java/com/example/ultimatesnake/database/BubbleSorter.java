package com.example.ultimatesnake.database;

import java.util.List;

/**
 * A generic bubblesort algorithm which sorts a list of type <T>
 * @param <T> object to sort
 */
public class BubbleSorter<T extends Comparable<? super T>> {

    /**
     * Sorts the list
     * @param list
     */
    public void sort(List<T> list) {

        T[] inputArray = (T[]) new Comparable[list.size()];
        inputArray = list.toArray(inputArray);

        boolean isSorted;
        for (int i = 0; i < inputArray.length - 1; i++) {
            isSorted = true;

            for (int j = 1; j < inputArray.length - i; j++) {

                if (inputArray[j].compareTo(inputArray[j-1]) < 0) {
                    swap(inputArray, j, j - 1);
                }
                isSorted = false;
            }
            if (isSorted) {
                break;
            }
        }
        list.clear();
        for(T element: inputArray) {
            list.add(element);
        }
    }

    /**
     * @return class name
     */
    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    /**
     * Swaps places of object[index1] with object[index2] in the array inputArray[]
     * @param inputArray
     * @param index1
     * @param index2
     */
    public void swap(T[] inputArray, int index1, int index2) {
        T temp = inputArray[index1];
        inputArray[index1] = inputArray[index2];
        inputArray[index2] = temp;
    }
}

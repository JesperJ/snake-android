package com.example.ultimatesnake.database;

/**
 * Class representing a highcore with a name and a score
 */
public class HighScore implements Comparable<HighScore> {

    private String name;
    private int highScore;

    /**
     * Creates a new HighScore object
     * @param name
     * @param highScore
     */
    public HighScore(String name, int highScore) {
        this.name = name;
        this.highScore = highScore;
    }

    public HighScore(){}

    /**
     * Returns the name of the specific highscore
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the score of the specific highscore
     * @return int
     */
    public int getHighScore() {
        return highScore;
    }

    /**
     * Creates and returns a string representation of the highscore
     * with a name and its score
     * @return String
     */
    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", highScore=" + highScore +
                '}';
    }

    /**
     * Compares the current highscore with another highscore
     * @param highScore
     * @return 0 if equal, 1 if current is higher, -1 if current is lower
     *
     */
    @Override
    public int compareTo(HighScore highScore) {
        if (this.highScore == highScore.getHighScore()) {
            return 0;
        } else if (this.highScore > highScore.getHighScore()) {
            return 1;
        } else {
            return -1;
        }
    }
}

package com.example.ultimatesnake;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.ultimatesnake.game.GameActivity;

/**
 * A fragment which displays three different difficulty options
 * Starts GameActivity and forwards the choice
 */
public class DifficultyFragment extends Fragment {

    private Button easy;
    private Button normal;
    private Button hard;

    private int difficulty;

    /**
     * Creates a fragment and gather its current argument
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Create its view, connect its view-components and set listners to each
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_difficulty, container, false);
        easy = view.findViewById(R.id.easy);
        normal = view.findViewById(R.id.normal);
        hard = view.findViewById(R.id.hard);

        easy.setOnClickListener(e -> easyGame());
        normal.setOnClickListener(e -> normalGame());
        hard.setOnClickListener(e -> hardGame());
        return view;
    }

    /**
     * Sets the difficulty to 2 (hard) and calls startGame() method
     */
    private void hardGame() {
        difficulty = 2;
        startGame();
    }
    /**
     * Sets the difficulty to 1 (normal) and calls startGame() method
     */
    private void normalGame() {
        difficulty = 1;
        startGame();
    }

    /**
     * Sets the difficulty to 0 (childs play) and calls startGame() method
     */
    private void easyGame() {
        difficulty = 0;
        startGame();
    }

    /**
     * Start GameActivity with both difficulty- and color-value
     */
    private void startGame(){
        Intent game = new Intent(getContext(), GameActivity.class);
        game.putExtra("difficulty", difficulty);
        startActivity(game);
    }
}
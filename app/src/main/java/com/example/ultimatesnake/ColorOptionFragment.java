package com.example.ultimatesnake;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Shader;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;


/**
 * A fragment which allow a user to change the color of the snake
 * Uses a bundle to store the selected value
 */
public class ColorOptionFragment extends Fragment {
    private int[] colors = {R.drawable.pinksnake, R.drawable.redsnake, R.drawable.greensnake, R.drawable.bluesnake, R.drawable.cosmicsnake};
    private ImageView snake;
    private int count;

    private Button left;
    private Button right;
    private Button back;

    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    /**
     * Create a fragment and checks the shared preferences if a previously color was chosen
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = getActivity().getSharedPreferences("snakeColor", Activity.MODE_PRIVATE);
        editor = sp.edit();
        count = sp.getInt("snakeColor", 0);

    }

    /**
     * Create its view and connect its view-components with listeners
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return view that was created
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_color_option, container, false);
        right = view.findViewById(R.id.rightArrow);
        left = view.findViewById(R.id.leftArrow);
        right.setOnClickListener(e -> rightClick());
        left.setOnClickListener(e -> leftClick());
        back = view.findViewById(R.id.backButton);
        back.setOnClickListener(e -> backPressed());

        snake = view.findViewById(R.id.image_view);
        snake.setImageResource(colors[count]);
        return view;
    }

    private void backPressed() {
        getActivity().onBackPressed();
    }

    /**
     * Update the count of the possible snake colors
     * If the count of colors is 0 it gets set to the last color (index 3)
     * Saves the count in a shared preferences
     */
    private void leftClick() {
        if(count == 0){
            count = 5;
        }
        count--;
        snake.setImageResource(colors[count]);
        editor.putInt("snakeColor", count);
        editor.apply();

    }

    /**
     * Update the count of the possible snakecolors
     * If the count of colors is 3 it gets set to the first color (index 0)
     * Saves the count in a shared preferences
     */
    private void rightClick() {
        if(count == 4){
            count = -1;
        }
        count++;
        snake.setImageResource(colors[count]);
        editor.putInt("snakeColor", count);
        editor.apply();

    }
}
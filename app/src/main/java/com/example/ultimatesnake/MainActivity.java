package com.example.ultimatesnake;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.example.ultimatesnake.animation.AnimatedSnakeView;
import com.example.ultimatesnake.game.Direction;
import com.example.ultimatesnake.game.Rectangle;
import com.example.ultimatesnake.game.StopGameInterface;
import com.example.ultimatesnake.game.model.SnakeGame;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MusicStoppedListener, StopGameInterface {

    private Button start;
    private Button option;
    private Button stats;
    private Button colorChange;
    private Button about;


    private String audioLink = "https://dl.dropboxusercontent.com/s/a9b85hbl8yiturn/snakeflute.mp3?dl=0";

    private boolean musicPlaying = false;
    private boolean gameMusicIsPlaying = false;
    private Intent serviceIntent;
    private SharedPreferences sp;

    /**
     * Creates the main menu of the game and sets listeners to each button, creates an
     * instance of the model to run as a simulated version.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        start = findViewById(R.id.button_start);
        start.setOnClickListener(e -> startGame());
        option = findViewById(R.id.button_option);
        option.setOnClickListener(e -> startOption());
        stats = findViewById(R.id.button_stats);
        stats.setOnClickListener(e -> startStats());
        colorChange = findViewById(R.id.button_changeSnake);
        colorChange.setOnClickListener(e -> colorChange());
        about = findViewById(R.id.button_about);
        about.setOnClickListener(e -> startAbout());
        serviceIntent = new Intent(this, MyPlayService.class);

        game = new SnakeGame(0, 200, 250);
        gameView = findViewById(R.id.animated_canvas);
        ApplicationClass.context = (Context) MainActivity.this;

        sp = getSharedPreferences("snakeColor", Activity.MODE_PRIVATE);
    }

    /**
     * Calls replaceFragment() to replace the current fragment to this.
     */
    private void colorChange() {
        replaceFragment(new ColorOptionFragment());
    }

    /**
     * Replaces the current fragment inside the container with the
     * passed on
     * @param fragment
     */
    private void replaceFragment(Fragment fragment){
        FragmentManager fragmentManager= getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.startsida,fragment);
        ft.addToBackStack("hello");
        ft.commit();
        start.setClickable(false);
        stats.setClickable(false);
        option.setClickable(false);
        about.setClickable(false);
        colorChange.setClickable(false);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        start.setClickable(true);
        stats.setClickable(true);
        option.setClickable(true);
        about.setClickable(true);
        colorChange.setClickable(true);
    }

    /**
     * Starts the music
     */
    @Override
    protected void onStart() {
        super.onStart();
        onMusic();
    }

    /**
     * Starts the music if its off, and turns it off if its on
     */
    private void onMusic() {
        SharedPreferences sharedPreferences = getSharedPreferences("music", Activity.MODE_PRIVATE);
        musicPlaying = sharedPreferences.getBoolean("musicState",musicPlaying);
        gameMusicIsPlaying = sharedPreferences.getBoolean("gameMusicState", gameMusicIsPlaying);
        if(musicPlaying || gameMusicIsPlaying){
            stopPlayAudio();
        }else{
            playAudio();
        }
    }

    /**
     * Turns off the music
     */
    private void stopPlayAudio() {
        try {
            stopService(serviceIntent);
        }catch (SecurityException e){
            Toast.makeText(this, "Error: "+ e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Starts the music
     */
    private void playAudio() {
        serviceIntent.putExtra("AudioLink",audioLink);

        try {
            startService(serviceIntent);
        }catch (SecurityException e){
            Toast.makeText(this, "Error: "+ e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Listener to the button 'stats', calls the method replaceFragment to
     * replace the current fragment to this (StatFragment)
     */
    public void startStats() {
        replaceFragment(new StatFragment());
    }

    /**
     * Listener to the button 'options' calls the method replaceFragment to
     * replace the current fragment to this (OptionFragment)
     */
    public void startOption() {
        replaceFragment(new OptionFragment());
    }

        /**
         * Listener to the button 'start'
         * Calls replaceFragment to replace the current fragment with this
         * (DifficultyFragment)
         */
    private void startGame() {
        replaceFragment(new DifficultyFragment());
    }

    private void startAbout(){ replaceFragment(new AboutFragment());}

    /**
     * Checks if the color of the snake has been changed
     * @return int which represent a specific color in the Color resource
     */
    public int getSnakeColor(){
        int snakeColor = sp.getInt("snakeColor", 0);
        if(snakeColor == 0){
             return getResources().getColor(R.color.snakeColorOption1);
        } else if (snakeColor == 1){
            return getResources().getColor(R.color.snakeColorOption0);
        } else if(snakeColor == 2){
            return getResources().getColor(R.color.snakeColorOption2);
        } else if (snakeColor == 3){
            return getResources().getColor(R.color.snakeColorOption3);
        }
        return 4;
    }
    /**
     * Sets the variable musicPlaying to false when the music stops
     */
    @Override
    public void onMusicStopped() {
        musicPlaying = false;
    }

    private AnimatedSnakeView gameView; // View
    private SnakeGame game;     // Model

    Runnable runnable;  // Innehåller koden som körs varje uppdatering
    Handler handler = new Handler(); // Kör runnable och pausar den när appen stängs
    final int updateFrequency = 20; // milliseconds
    private int count = 0;

    /**
     * Simulates a predetermined execution of the game, uses
     * a instance of class SnakeGame
     */
    @Override
    protected void onResume() {
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {
                if(count == 70){
                    game.setDirection(Direction.DOWN);
                }
                if(count == 120){
                    game.setDirection(Direction.RIGHT);
                }
                if (count == 140){
                    game.setDirection(Direction.UP);
                }
                if (count == 200){
                    game.setDirection(Direction.RIGHT);
                }
                if(count == 260){
                    game.setDirection(Direction.UP);
                }
                if (count == 300){
                    game.setDirection(Direction.RIGHT);
                }
                if(count == 320){
                    game.setDirection(Direction.DOWN);
                }
                if(count == 370){
                    game.setDirection(Direction.LEFT);
                }
                if(count == 385){
                    game = new SnakeGame(0, 200, 250);
                    count = -0;
                }
                updateModel();
                count++;
                gameView.invalidate();  // invalidate får view:n att ritas om
                handler.postDelayed(this, updateFrequency);
            }
        }, updateFrequency);
        onMusic();
        super.onResume();
    }

    /**
     * Stops music and remove all callback to method run() to pause
     * the current simulation
     */
    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable);
        stopPlayAudio();
        super.onPause();
    }

    @Override
    protected void onStop(){
        super.onStop();
        stopPlayAudio();
    }

    /**
     * @return List<Rectangle> of the snakes components
     */
    public List<Rectangle> getSnake() {
        return game.getSnake();
    }

    /**
     * @return int representing the height of the game
     */
    public int getLevelHeight() {
        return game.getLevelHeight();
    }

    /**
     * @return int representing the width of the game
     */
    public int getLevelWidth() {
        return game.getLevelWidth();
    }

    /**
     * @return List<Rectangle> of all current food
     */
    public List<Rectangle> getFood() { return game.getFoods(); }

    /**
     * Uppdates the model
     */
    private void updateModel() {
        game.update();
    }

    @Override
    public void stopGame() {
        stopPlayAudio();
    }

    @Override
    public void resumeGame() {
        onMusic();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }
}

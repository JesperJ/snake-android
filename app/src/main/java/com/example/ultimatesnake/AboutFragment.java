package com.example.ultimatesnake;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class AboutFragment extends Fragment {

    private Button back;
    private TextView about, powerUps, redPowerUp, bluePowerUp, yellowPowerUp, gameMechanics, gameMechanicsText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_about, container, false);
        back = view.findViewById(R.id.backButton);
        back.setOnClickListener(e -> backPressed());
        about = view.findViewById(R.id.about);
        powerUps = view.findViewById(R.id.power_ups);
        redPowerUp = view.findViewById(R.id.red_power_up);
        bluePowerUp = view.findViewById(R.id.blue_power_up);
        yellowPowerUp = view.findViewById(R.id.yellow_power_up);
        gameMechanics = view.findViewById(R.id.game_mechanics);
        gameMechanicsText = view.findViewById(R.id.game_mechanics_text);

        return view;
    }

    private void backPressed() {
        getActivity().onBackPressed();
    }
}
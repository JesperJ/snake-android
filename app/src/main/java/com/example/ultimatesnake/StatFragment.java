package com.example.ultimatesnake;


import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.example.ultimatesnake.database.HighScore;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;

/**
 * A fragment which retrieves all the top 5 highscores stored in the database
 * for each difficulty level and displays it to the user
 */
public class StatFragment extends Fragment {
    private TextView easyTitle, normalTitle, hardTitle;
    private TextView playerNameEasy, playerNamesNormal, playerNamesHard;
    private TextView topHighScoresEasy, topHighScoresNormal, topHighScoresHard;

    private String topHighScores;
    private String topNames;
    private List<HighScore> currentValues;

    private FirebaseDatabase database;
    private DatabaseReference databaseHighScoreReferenceEasy;
    private DatabaseReference databaseHighScoreReferenceNormal;
    private DatabaseReference databaseHighScoreReferenceHard;

    private Button back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stats, container, false);
        if(view != null){
            easyTitle = view.findViewById(R.id.childs_play_highscore_banner);
            normalTitle = view.findViewById(R.id.normal_highscore_banner);
            hardTitle = view.findViewById(R.id.hard_highscore_banner);
            playerNameEasy = view.findViewById(R.id.output_name_childs_play);
            topHighScoresEasy = view.findViewById(R.id.output_highscore_childs_play);
            playerNamesNormal = view.findViewById(R.id.output_name_normal);
            topHighScoresNormal = view.findViewById(R.id.output_highscore_normal);
            playerNamesHard = view.findViewById(R.id.output_name_hard);
            topHighScoresHard = view.findViewById(R.id.output_highscore_hard);

            back = view.findViewById(R.id.backButton);
            back.setOnClickListener(e -> backPressed());

            database = FirebaseDatabase.getInstance("https://highscorertdb-default-rtdb.europe-west1.firebasedatabase.app/");
            databaseHighScoreReferenceEasy = database.getReference("EasyHighscore");
            databaseHighScoreReferenceNormal = database.getReference("NormalHighscore");
            databaseHighScoreReferenceHard = database.getReference("HardHighscore");

            databaseHighScoreReferenceEasy.addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {

                        currentValues = new ArrayList<>();
                        for (DataSnapshot value : dataSnapshot.getChildren()) {
                            currentValues.add(value.getValue(HighScore.class));
                        }
                        topHighScores = "";
                        topNames = "";
                        int counter = 1;
                        for (HighScore i : currentValues) {
                            topHighScores += i.getHighScore() + "\n";
                            topNames = topNames + counter + " " + i.getName() + "\n";
                            counter++;
                        }
                        playerNameEasy.setText(topNames);
                        topHighScoresEasy.setText(topHighScores);
                    }
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    String TAG = "something went wrong";
                    Log.w(TAG, "Failed to read value.", error.toException());
                }
            });

            databaseHighScoreReferenceNormal.addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {

                        currentValues = new ArrayList<>();
                        for (DataSnapshot value : dataSnapshot.getChildren()) {
                            currentValues.add(value.getValue(HighScore.class));
                        }
                        topHighScores = "";
                        topNames = "";
                        int counter = 1;
                        for (HighScore i : currentValues) {
                            topHighScores += i.getHighScore() + "\n";
                            topNames = topNames + counter + " " + i.getName() + "\n";
                            counter++;
                        }
                        playerNamesNormal.setText(topNames);
                        topHighScoresNormal.setText(topHighScores);
                    }
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    String TAG = "something went wrong";
                    Log.w(TAG, "Failed to read value.", error.toException());
                }
            });

            databaseHighScoreReferenceHard.addValueEventListener(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {

                        currentValues = new ArrayList<>();
                        for (DataSnapshot value : dataSnapshot.getChildren()) {
                            currentValues.add(value.getValue(HighScore.class));
                        }
                        topHighScores = "";
                        topNames = "";
                        int counter = 1;
                        for (HighScore i : currentValues) {
                            topHighScores += i.getHighScore() + "\n";
                            topNames = topNames + counter + " " + i.getName() + "\n";
                            counter++;
                        }
                        playerNamesHard.setText(topNames);
                        topHighScoresHard.setText(topHighScores);
                    }
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    String TAG = "something went wrong";
                    Log.w(TAG, "Failed to read value.", error.toException());
                }
            });
        }
        return view;
    }

    private void backPressed() {
        getActivity().onBackPressed();
    }

    @Override
    public void onStart(){
        super.onStart();
    }




    @Override
    public void onDestroy(){
        super.onDestroy();
    }


}